export const USERS_FETCH_PENDING = "USERS_FETCH_PENDING";

export const USERS_FETCH_SUCCESS = "USERS_FETCH_SUCCESS";

export const USERS_FETCH_ERROR = "USERS_FETCH_ERROR";

export const USERS_PAGE_CHANGE = "USERS_PAGE_CHANGE";

export const USERS_CLICK_DETAIL = "NGƯỜI DUNG CLICK VÀO NÚT CHI TIẾT";

export const CLOSE_MODAL = "đóng modal";

export const CHANGE_ROW_NUMBER = "Thay đổi số hàng";