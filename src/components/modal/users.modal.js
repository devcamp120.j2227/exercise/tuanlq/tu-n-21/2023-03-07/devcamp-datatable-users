import Modal from '@mui/material/Modal';
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';
import { useDispatch, useSelector } from "react-redux";
import { closeModal } from '../../actions/users.actions';
export default function Modal1 (){
  const dispatch = useDispatch();
  const { userDetail, modal} = useSelector((reduxData) => reduxData.usersReducers);
    const style = {
        position: 'absolute',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        width: 400,
        bgcolor: 'background.paper',
        boxShadow: 24,
        p: 4,
        borderRadius: "10px"
      };
    const handlerCloseModal = () => {
      dispatch(closeModal())
    }
    return (
      <div>
        <Modal
          keepMounted
          open = {modal}
          onClose = {handlerCloseModal}
          aria-labelledby="keep-mounted-modal-title"
          aria-describedby="keep-mounted-modal-description"
        >
          <Box sx={style}>
            <Typography id="keep-mounted-modal-title" variant="h6" component="h2">
              Chi tiết thông tin khách hàng
            </Typography>
            <Typography id="keep-mounted-modal-description" sx={{ mt: 2 }}>
                Id : {userDetail.id}
            </Typography>
            <Typography id="keep-mounted-modal-description" sx={{ mt: 2 }}>
                Firstname : {userDetail.firstname}
            </Typography>
            <Typography id="keep-mounted-modal-description" sx={{ mt: 2 }}>
                Lastname : {userDetail.lastname}
            </Typography>
            <Typography id="keep-mounted-modal-description" sx={{ mt: 2 }}>
                Country : {userDetail.country}
            </Typography>
            <Typography id="keep-mounted-modal-description" sx={{ mt: 2 }}>
                Subject : {userDetail.subject}
            </Typography>
            <Typography id="keep-mounted-modal-description" sx={{ mt: 2 }}>
                CustomerType : {userDetail.customerType}
            </Typography>
            <Typography id="keep-mounted-modal-description" sx={{ mt: 2 }}>
                Register Status : {userDetail.registerStatus}
            </Typography>
            
          </Box>
        </Modal>
      </div>
    );
}