import {
    USERS_FETCH_ERROR,
    USERS_FETCH_PENDING,
    USERS_FETCH_SUCCESS,
    USERS_PAGE_CHANGE,
    USERS_CLICK_DETAIL, 
    CLOSE_MODAL,
    CHANGE_ROW_NUMBER
} from "../constants/users.constants";

export const fetchUsers = (page, limit) => {
    return async (dispatch) => {
        try {
            var requestOptions = {
                method: 'GET',
                redirect: 'follow'
            };
    
            await dispatch({
                type: USERS_FETCH_PENDING
            })

            const responseTotalUser = await fetch("http://203.171.20.210:8080/devcamp-register-java-api/users", requestOptions);

            const dataTotalUser = await responseTotalUser.json();
            // console.log( dataTotalUser)
            
            const params = new URLSearchParams({
                page: page-1, 
                size: limit
            });
        
            const response = await fetch("http://203.171.20.210:8080/devcamp-register-java-api/user-list-pagination?" + params.toString(), requestOptions);

            const data = await response.json();
            
            return dispatch({
                type: USERS_FETCH_SUCCESS,
                totalUser: dataTotalUser.length,
                data: data
            })
        } catch (error) {
            return dispatch({
                type: USERS_FETCH_ERROR,
                error: error
            })
        }   
    }
}

export const pageChangePagination = (page) => {
    return {
        type: USERS_PAGE_CHANGE,
        page: page
    }
}

export const showModalUserDetail = (user) => {
    return {
        type: USERS_CLICK_DETAIL,
        page: user
    }
}
export const closeModal = () => {
    return{
        type: CLOSE_MODAL
    }
}

export const changeRowNumber = (Rows) => {
    return{
        type: CHANGE_ROW_NUMBER,
        page: Rows
    }
}