import {
    USERS_FETCH_ERROR,
    USERS_FETCH_PENDING,
    USERS_FETCH_SUCCESS,
    USERS_PAGE_CHANGE,
    USERS_CLICK_DETAIL, 
    CLOSE_MODAL, 
    CHANGE_ROW_NUMBER
} from "../constants/users.constants";

const initialState = {
    users: [],
    limit: 10,
    pending: false,
    error: null,
    noPage: 0,
    currentPage: 1,
    modal: false,
    userDetail:{}
}

const userReducers = (state = initialState, action) => {
    switch (action.type) {
        case USERS_FETCH_PENDING:
            state.pending = true;
            break;
        case USERS_FETCH_SUCCESS:
            state.pending = false;
            state.noPage = Math.ceil(action.totalUser / state.limit);
            state.users = action.data;
            break;
        case USERS_FETCH_ERROR:
            break;
        case USERS_PAGE_CHANGE:
            state.currentPage = action.page;
            break;
        case USERS_CLICK_DETAIL:
            state.userDetail = action.page;
            console.log(state.userDetail)
            state.modal = true;
            break;
        case CLOSE_MODAL:
            state.modal = false;
            break;
        case CHANGE_ROW_NUMBER:
            state.limit = action.page;
            console.log(state.limit)
            break;
        default:
            break;
    }

    return {...state};
}

export default userReducers;